<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2016-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = dirname(__DIR__);

use Vpn\Node\Config;
use Vpn\Node\FileIO;
use Vpn\Node\HttpClient\CurlHttpClient;
use Vpn\Node\HttpClient\Exception\HttpClientException;
use Vpn\Node\HttpClient\HttpClientRequest;

try {
    $nodeKeyFile = $baseDir . '/config/keys/node.key';
    $config = Config::fromFile($baseDir . '/config/config.php');

    // send a "ping" to nodeUrl
    $httpClient = new CurlHttpClient();
    $httpRequest = new HttpClientRequest(
        'GET',
        $config->apiUrl() . '/ping',
        [],
        [],
        [
            'X-Node-Number' => (string) $config->nodeNumber(),
            'Authorization' => 'Bearer ' . FileIO::read($nodeKeyFile),
        ]
    );
    $httpClient->send($httpRequest);
} catch (Exception $e) {
    $exceptionMessage = $e->getMessage();
    if ($e instanceof HttpClientException) {
        // add the HttpClientResponse body to error message
        $exceptionMessage .= (string) $e->httpClientResponse();
    }

    echo 'ERROR: ' . $exceptionMessage . \PHP_EOL;

    exit(1);
}
