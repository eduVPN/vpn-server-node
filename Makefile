.PHONY: test fmt psalm phpstan sloc
SRC_DIRS=src libexec

test:
	# try system wide installations of PHPUnit first
	/usr/bin/phpunit || /usr/bin/phpunit9 || phpunit

fmt:
	php-cs-fixer fix

psalm:
	psalm

phpstan:
	phpstan

sloc:
	tokei $(SRC_DIRS) || cloc $(SRC_DIRS)
